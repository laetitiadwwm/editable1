let data 
let tableElem = document.createElement('table');


function createTable(elem){

    let jsonURL = elem.getAttribute('data-source') 
    
    fetch(jsonURL)
    .then((response)=> response.json())
    .then((lesDatasDuFichier) => {
        data = lesDatasDuFichier 
        tableHeader()
        tableBody()
        });

    elem.appendChild(tableElem)
}

function tableHeader(){

    let unElement = data[0] 
    let tr = document.createElement('tr') // creer ligne du tableau
    for(let key in unElement){                   // for in parcours les clefs
        let th = document.createElement('th') // creer les titre
        th.innerText = key //inner egal zone de text
        th.onclick = function(){ // Onclick permet d'eefectuer une action en cliquant
            
            if(typeof(unElement[key]) === 'string'){
                data.sort(function (a,b) {
                    return a[key].localeCompare(b[key])   // faire un trie pour lettre  
                });  
            } else {
                data.sort(function(a, b) { // formule de comparaison pour trier
                    return a[key] - b[key]; // on trie les clefs  !!! fonctionne pour chiffre
                  });
            }
              tableBody()
        }

        tr.appendChild(th) // met les titres th dans le tr § TIPS lire à l'envers
    } 
    let thead= document.createElement('thead')
    thead.appendChild(tr)
    tableElem.appendChild(thead)
}


function tableBody(){
    tableElem.querySelector('tbody')?.remove()// Selectionne "tbody" et le supprime si il existe deja
    let tbody= document.createElement('tbody') // creer le "tbody"

    for(let obj of data){        // for of parcour les valeurs
        let tr = document.createElement('tr')
        for(let key in obj){    //pour les clefs dans l'objets
            let td = document.createElement('td')
            td.className = "table-data"
            td.contentEditable = true; // rend le contenu des cellules editables
            td.innerText = obj[key] //innerText defini une zone de texte
            tr.appendChild(td)
        }
        tbody.appendChild(tr)
    }
    tableElem.appendChild(tbody)

    
        // Get the input element with id "search-input"
        let input = document.getElementById("barredefiltre");
    
        // Add an event listener to the input element that listens for "keyup" events
        input.addEventListener("keyup", filtrerTableau);

    

    
    function filtrerTableau() {
        let input = document.getElementById("barredefiltre").value.toLowerCase ();// obtient la valeur de l'input avec l'ID barredefiltre. converti en minuscule.   
        let tbody = tableElem.getElementsByTagName("tbody")[0]; //prend le 1er element de tbody dans la table
        let tr = tbody.getElementsByTagName('tr'); // prend tout les elements dans les tr du tableau tbody

        for (i = 0; i< tr.length; i++) { //boucle a pas faire - boucle sur les element tr
            let Cellules= tr[i].getElementsByTagName("td"); // prend les element td dans le tr
            let match = false; // pas de match dans la ligne 

            for (let j = 0; j < Cellules.length; j++) { //boucle sur element de cellules
                if (Cellules[j].innerText.toLowerCase().indexOf(input)> -1) { //verifie si les caractéres dans l'input sont dans le tableau
                    match = true; // match doit etre vrai
                    break;
                }
            }
            if (match){ //si y'a correspondance
                tr[i].style.display = ""; //ajoute les matchs dans les guillemet = ressort les ligne dans le tableau
            }
            else {
                tr[i].style.display = "none"; //si pas de match rien
            }
        }
    }
// PAS FINI  
    // function ligneplus () {
    //     let tprice = document.getElementById("price").value;
    //     let tname = document.getElementById ("name").value;
    //     let tavailable = document.getElementById("available").value;
    //     let ttag = document.getElementById("tag").value;
    // }

    //         let recup={

    //             name:tname,
    //             price: tprice,
    //             available: tavailable,
    //             tag: ttag
    //         }

    //         data.push(recup)
    //         console.log(data)
    //         tableBody();
}













//===================================================================================================
                                      //test 
                                      
// let dataJson = JSON.stringify(data)
// fetch('../test/up.php',{
//     method: 'GET',
//     body: dataJson,
// })

// saveButton.onclick= function update () {
//     (async () => {
//         const rawResponse = await fetch('../test/up.php', {
//         method: 'POST',
//         headers: {
//             'Accept': 'application/json',
//             'Content-Type': 'application/json'
//         },
//         body: JSON.stringify({a: 1, b: 'Textual content'})
//         });
//         const content = await rawResponse.json();
//         return content;
//     });
// }

